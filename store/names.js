export const state = () => ({
  list: [
    {
      id: 1,
      name: 'Ben Doe'
    }
  ]
})

export const mutations = {
  populateRooms(state, data) {
    return state.list.push(data)
  },
  updateName(state, data) {
    state.list.map(name => {
      if (name.id === data.id) {
        return (state.list[data.id - 1] = data)
      }
    })
  },
  removeName(state, data) {
    let id = 1
    state.list.splice(data, 1)

    return state.list.map(name => {
      name.id = id
      id += 1
    })
  }
}

export const actions = {}
