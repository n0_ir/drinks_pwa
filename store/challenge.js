import challenges from '~/data/all.json'

export const state = () => ({
  list: []
})

export const mutations = {
  populateChallenges(state, data) {
    return (state.list = data)
  }
}

export const actions = {
  POPULATE_CHALLENGES({ commit }) {
    commit('populateChallenges', challenges)
  }
}
